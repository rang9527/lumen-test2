<?php

namespace App\Events;

use App\Models\Car;

class ExampleEvent extends Event
{

	public $car;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Car $car)
    {
        $this->car = $car;
    }
}
