<?php
namespace App\Http\Controllers;

use App\Models\Car;
use Illuminate\Http\Request;
use Redis;
use DB;
use Queue;
use App\Jobs\ExampleJob;
use App\Events\ExampleEvent;

class CarController extends Controller
{
    
    public function createCar(Request $request)
    {
        $this->validate($request, [
            'make' => 'required|max:50',
            'year' => 'required|numeric'
        ]);

        // Redis::set('name', 'Roy');

        // $data = Redis::get('name');

        // $data = DB::select("SELECT * FROM cars");

        // $test = Db::select("SELECT * FROM test.user");

        // var_dump($test);

        // dd($data);

        // dd($request->all());

        $car = Car::create($request->all());
        return response()->json($car);
    }

    public function updateCar(Request $request, $id)
    {
        $car = Car::find($id);
        $car->make = $request->input('make');
        $car->model = $request->input('model');
        $car->year = $request->input('year');
        $car->save();

        return response()->json($car);
    }

    public function deleteCar($id)
    {
        $car = Car::find($id);
        $car->delete();

        return response()->json('删除成功');
    }

    public function index()
    {
        $cars = Car::all();
        return response()->json($cars);
    }

    // 测试推送
    public function push($id)
    {

        $car = Car::findOrFail($id);
        Queue::push(new ExampleJob($car));

    }


    // 测试事件
    public function event($id)
    {
        $car = Car::findOrFail($id);
        event(new ExampleEvent($car));
    }

}