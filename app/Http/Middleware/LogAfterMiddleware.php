<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\ResponseLog;

/**
 * after request 记录日志
 *
 * @author roy.cai
 */

class LogAfterMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $logs = [
            'status' => $response->status(),
            'content' => $response->content()
        ];

        ResponseLog::create($logs);

        return $response;
    }
}
