<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\RequestLog;

/**
 * before request 记录日志
 *
 * @author roy.cai
 */

class LogBeforeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $logs = [
            'path' => $request->path(),
            'method' => $request->method(),
            'url' => $request->fullUrl(),
            'params' => http_build_query($request->all())
        ];
        RequestLog::create($logs);
        return $next($request);
    }
}
