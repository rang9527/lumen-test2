<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/




// 配置信息实例
$app->get('/timezone',function() use ($app){
	return config('app.timezone');
});

// 访问频率限制
$app->group(['middleware' => 'throttle:2,1'], function () use ($app) {
    $app->get('/', function () use ($app) {
        return $app->version();
    });
});

// 事件测试
$app->get('car/{id}/event', 'CarController@event');

// 推送任务
$app->get('car/{id}/push','CarController@push');

// 错误日志记录
$app->get('log',function() use ($app){
    LOG::info('info ... debug');
});


// 汽车模型RESTful
$app->group(['prefix' => 'api/v1','middleware' => ['throttle:60,1','logBefore','logAfter']], function($app)
{
    $app->post('car','CarController@createCar');
    $app->put('car/{id}','CarController@updateCar');
    $app->delete('car/{id}','CarController@deleteCar');
    $app->get('car','CarController@index');
});