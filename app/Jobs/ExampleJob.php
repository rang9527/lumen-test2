<?php

namespace App\Jobs;

use App\Models\Car;

class ExampleJob extends Job
{
    protected $car;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Car $car)
    {
        $this->car = $car;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $car = $this->car;

        var_dump('job ',$car->make);

        // 写入文件
        // file_put_contents('D:\testfile.txt', $car->make."\r\n",FILE_APPEND);

    }
}
