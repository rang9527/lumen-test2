<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequestLog extends Model
{
	protected $table = 'request_logs';
	
	protected $fillable = ['path', 'method', 'url', 'type','params', 'created_at', 'updated_at'];

}