<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResponseLog extends Model
{
	// protected $table = 'response_logs';

	protected $fillable = ['status', 'content', 'created_at', 'updated_at'];

}