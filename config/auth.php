<?php

return [
	'guards' => [
	    'api' => [
	        'driver' => 'basic',
	        'provider' => 'users'
	    ],

	    // ...
	],

	'providers' => [
	    'users' => [
	        'driver' => 'eloquent',
	        'model'  => App\User::class,
	    ],
	],

];