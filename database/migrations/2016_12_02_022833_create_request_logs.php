<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('path',100)->comment('路径');
            $table->enum('method', ['GET', 'HEAD','POST','PUT','DELETE','CONNECT','OPTIONS','TRACE','PATCH'])->comment('请求方法');
            $table->string('url')->comment('url');
            $table->string('params',2000)->comment('参数');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('request_logs');
    }
}
