Lumen：为速度而生的 Laravel 框架
-----------------------

 - 官方文档：https://lumen.laravel.com/docs/5.3 （英文）
 - 中文翻译：https://lumen.laravel-china.org/docs/5.3 （中文）

## 测试文档相关 ##

 - Swagger 官网：http://swagger.io/
 - Swagger 编辑器：http://editor.swagger.io/
 - YAML 语法：http://www.ruanyifeng.com/blog/2016/07/yaml.html

## LumenRestApi 项目 ##


本项目是基于Lumen 5.3版本开发，目前为1.0版本。

增加以下功能：

 1. Car 汽车模型的基础实例
 2. Swagger 文档及测试
 3. Redis 配置
 4. Queue 配置
 5. Command 配置
 6. Database 配置
 7. 添加 Throttle 接口访问频率设置中间件
 8. 添加接口预处理、后处理日志中间件